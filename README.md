# goapp

A sample Go app that will respond to a web request with 

Hi there, I'm served from _node hostname_!

## Setup

Install golang

    # ubuntu
    apt-get install golang
    # mac osx (requires homebrew)
    brew install go --cross-compile-common

Configure a workspace directory

    mkdir -p ~/go

Create the required environment variables

    export GOPATH=~/go
    export GOBIN=~/go/bin
    # optionally add these to your .bashrc

Get, build and install the application

    go get github.com/halberom/goapp

Run it

    go /opt/go/bin/goapp

Browse to http://ipaddress:8484
